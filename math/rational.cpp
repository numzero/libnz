/**
 * © Copyright 2018 Lobachevskiy Vitaliy <numzer0@yandex.ru>
 *
 * This file is part of LibNZ.
 *
 * LibNZ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. *
 *
 * LibNZ is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR a PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with LibNZ.  If not, see <https://www.gnu.org/licenses/>
 *
 * @file math/rational.cpp
 * @author Lobachevskiy Vitaliy <numzer0@yandex.ru>
 * @copyright GNU LGPL v3+
 */

#include "rational.hpp"
#include <stdexcept>

namespace nz {
namespace math {

template<typename Integer, typename T>
basic_rational<Integer, T>::basic_rational(integer const &val) noexcept
	: basic_rational(val, 1)
{
}

template<typename Integer, typename T>
basic_rational<Integer, T>::basic_rational(integer const &_num, integer const &_den)
	: num(_num), den(_den)
{
	normalize();
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator= (integer const &val) noexcept -> basic_rational &
{
	num = val;
	den = 1;
	return *this;
}

template<typename Integer, typename T>
void basic_rational<Integer, T>::assign(integer const &_num, integer const &_den)
{
	num = _num;
	den = _den;
	normalize();
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::numer() const noexcept -> integer const &
{
	return num;
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::denom() const noexcept -> integer const &
{
	return den;
}

template<typename Integer, typename T>
basic_rational<Integer, T>::operator bool() const noexcept
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::negate() noexcept -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::invert() noexcept -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator+= (integer const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator-= (integer const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator*= (integer const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator/= (integer const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator+= (basic_rational const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator-= (basic_rational const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator*= (basic_rational const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::operator/= (basic_rational const &rhs) -> basic_rational &
{
}

template<typename Integer, typename T>
auto basic_rational<Integer, T>::gcd(integer a, integer b) -> integer
{
	if (a < 0)
		a = -a;
	if (b < 0)
		b = -b;
	if (a < b)
		std::swap(a, b);
	while (b) {
		integer c = a % b;
		a = b;
		b = c;
	}
	return a;
}

template<typename Integer, typename T>
void basic_rational<Integer, T>::normalize()
{
	if (!den)
		throw std::invalid_argument("Attempt to create a rational with zero denominator");
	if (den < 0) {
		num = -num;
		den = -den;
	}
	integer d = gcd(num, den);
	num /= d;
	den /= d;
}

template <typename integer>
basic_rational<integer> operator+ (basic_rational<integer> const &val)
{
	return val;
}

template <typename integer>
basic_rational<integer> operator- (basic_rational<integer> const &val)
{
	return {-val.numer(), val.denom()};
}

template <typename integer>
basic_rational<integer> operator+ (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return {lhs.numer() * rhs.denom() + rhs.numer() * lhs.denom(),
		lhs.denom() * rhs.denom()};
}

template <typename integer>
basic_rational<integer> operator- (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return {lhs.numer() * rhs.denom() - rhs.numer() * lhs.denom(),
		lhs.denom() * rhs.denom()};
}

template <typename integer>
basic_rational<integer> operator* (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return {lhs.numer() * rhs.numer(),
		lhs.denom() * rhs.denom()};
}

template <typename integer>
basic_rational<integer> operator/ (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return {lhs.numer() * rhs.denom(),
		lhs.numer() * rhs.denom()};
}

template <typename integer>
bool operator== (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return (lhs.numer() == rhs.numer())
		&& (lhs.denom() == rhs.denom());
}

template <typename integer>
bool operator!= (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return (lhs.numer() != rhs.numer())
		|| (lhs.denom() != rhs.denom());
}

template <typename integer>
bool operator< (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return lhs.numer() * rhs.denom() < rhs.numer() * lhs.denom();
}

template <typename integer>
bool operator> (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return lhs.numer() * rhs.denom() > rhs.numer() * lhs.denom();
}

template <typename integer>
bool operator<= (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return lhs.numer() * rhs.denom() <= rhs.numer() * lhs.denom();
}

template <typename integer>
bool operator>= (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs)
{
	return lhs.numer() * rhs.denom() >= rhs.numer() * lhs.denom();
}

// template <typename integer>
// integer floor(basic_rational<integer> const &val)
// {
// }
//
// template <typename integer>
// integer ceil(basic_rational<integer> const &val)
// {
// }

template <typename integer>
integer trunc(basic_rational<integer> const &val)
{
	return val.numer() / val.denom();
}

template <typename integer, typename int2, class> basic_rational<integer>
modf(basic_rational<integer> const &val, int2 *intptr)
{
	basic_rational<integer> result {val.numer() % val.denom(), val.denom()};
	if (intptr)
		*intptr = val.numer() / val.denom();
	return result;
}

template <typename integer> basic_rational<integer> rem(basic_rational<integer> const &val)
{
	integer const &den = val.denom();
	integer const num = val.numer() % den;
	if (num < 0)
		num = den - num; // now positive
	return {num, den};
}

template <typename integer>
basic_rational<integer> abs(basic_rational<integer> const &val)
{
	integer const &num = val.numer();
	integer const &den = val.denom();
	if (num < 0)
		return {-num, den};
	return {num, den};
}

template <typename integer>
basic_rational<integer> reciprocal(basic_rational<integer> const &val)
{
	return {val.denom(), val.numer()};
}

// template<typename integer> basic_rational<integer> pow(basic_rational<integer> const &val, integer const &exp)
// {
// }

} // math namespace
} // nz namespace
