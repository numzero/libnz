/**
 * © Copyright 2018 Lobachevskiy Vitaliy <numzer0@yandex.ru>
 *
 * This file is part of LibNZ.
 *
 * LibNZ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. *
 *
 * LibNZ is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR a PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with LibNZ.  If not, see <https://www.gnu.org/licenses/>
 *
 * @file math/rational.cxx
 * @author Lobachevskiy Vitaliy <numzer0@yandex.ru>
 * @copyright GNU LGPL v3+
 */

#include "rational.hxx"
