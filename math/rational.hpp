/**
 * © Copyright 2018 Lobachevskiy Vitaliy <numzer0@yandex.ru>
 *
 * This file is part of LibNZ.
 *
 * LibNZ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. *
 *
 * LibNZ is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR a PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with LibNZ.  If not, see <https://www.gnu.org/licenses/>
 *
 * @file math/rational.hpp
 * @author Lobachevskiy Vitaliy <numzer0@yandex.ru>
 * @copyright GNU LGPL v3+
 */

#pragma once
#include <type_traits>

namespace nz {
namespace math {

template <typename Integer, class = typename std::enable_if<std::is_integral<Integer>::value>::type>
class basic_rational {
public:
	using integer = Integer;

public:
	basic_rational(basic_rational const &rat) = default;

// 	explicit basic_rational(float val);
// 	explicit basic_rational(double val);
// 	explicit basic_rational(long double val);

	basic_rational(integer const &val = 0) noexcept;
	basic_rational(integer const &_num, integer const &_den);

	~basic_rational() = default;

	basic_rational &operator= (basic_rational const &rat) = default;
	basic_rational &operator= (integer const &val) noexcept;
	void assign(integer const &_num, integer const &_den = 1);

// 	void swap(basic_rational &rhs) noexcept;

	integer const &numer() const noexcept;
	integer const &denom() const noexcept;

	explicit operator bool() const noexcept;

	basic_rational &negate() noexcept;
	basic_rational &invert() noexcept;

// 	basic_rational &operator++ ();
// 	basic_rational &operator-- ();

// 	basic_rational operator++ (int);
// 	basic_rational operator-- (int);

	basic_rational &operator+= (integer const &rhs);
	basic_rational &operator-= (integer const &rhs);
	basic_rational &operator*= (integer const &rhs);
	basic_rational &operator/= (integer const &rhs);

	basic_rational &operator+= (basic_rational const &rhs);
	basic_rational &operator-= (basic_rational const &rhs);
	basic_rational &operator*= (basic_rational const &rhs);
	basic_rational &operator/= (basic_rational const &rhs);

private:
	integer num, den;

	static integer gcd(integer a, integer b);
	void normalize();
};

template <typename integer> basic_rational<integer> operator+ (basic_rational<integer> const &val);
template <typename integer> basic_rational<integer> operator- (basic_rational<integer> const &val);

template <typename integer> basic_rational<integer> operator+ (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> basic_rational<integer> operator- (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> basic_rational<integer> operator* (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> basic_rational<integer> operator/ (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);

// template <typename integer> basic_rational<integer> operator+ (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> basic_rational<integer> operator- (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> basic_rational<integer> operator* (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> basic_rational<integer> operator/ (basic_rational<integer> const &lhs, integer const &rhs);
//
// template <typename integer> basic_rational<integer> operator+ (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> basic_rational<integer> operator- (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> basic_rational<integer> operator* (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> basic_rational<integer> operator/ (integer const &lhs, basic_rational<integer> const &rhs);

template <typename integer> bool operator== (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> bool operator!= (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> bool operator<  (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> bool operator>  (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> bool operator<= (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);
template <typename integer> bool operator>= (basic_rational<integer> const &lhs, basic_rational<integer> const &rhs);

// template <typename integer> bool operator== (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> bool operator!= (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> bool operator<  (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> bool operator>  (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> bool operator<= (basic_rational<integer> const &lhs, integer const &rhs);
// template <typename integer> bool operator>= (basic_rational<integer> const &lhs, integer const &rhs);

// template <typename integer> bool operator== (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> bool operator!= (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> bool operator<  (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> bool operator>  (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> bool operator<= (integer const &lhs, basic_rational<integer> const &rhs);
// template <typename integer> bool operator>= (integer const &lhs, basic_rational<integer> const &rhs);

enum class rounding_mode
{
	nearest_even, nearest_odd,
	toward_pos_inf, toward_neg_inf,
	toward_zero, away_from_zero,
};
template <typename integer> integer nearest(basic_rational<integer> const &val, rounding_mode mode = rounding_mode::nearest_even);
template <typename integer> integer floor(basic_rational<integer> const &val);
template <typename integer> integer ceil (basic_rational<integer> const &val);
template <typename integer> integer trunc(basic_rational<integer> const &val);
template <typename integer> integer round(basic_rational<integer> const &val);

template <typename integer, typename int2 = integer, class = typename std::enable_if<std::is_integral<int2>::value>::type> basic_rational<integer>
modf(basic_rational<integer> const &val, int2 *intptr = nullptr);
template <typename integer> basic_rational<integer> rem(basic_rational<integer> const &val);

template <typename integer> basic_rational<integer> abs(basic_rational<integer> const &val);
template <typename integer> basic_rational<integer> reciprocal(basic_rational<integer> const &val);
template<typename integer, typename T> basic_rational<integer> pow(basic_rational<integer> const&, T) = delete;
template<typename integer> basic_rational<integer> pow(basic_rational<integer> const &val, integer const &exp);

using rational = basic_rational<int>;

} // math namespace
} // nz namespace
