#pragma once
#include <stdexcept>
#include <asm/unistd.h>
#include "types.hxx"
#include "maybe.hxx"

namespace nz {
namespace unistd {

namespace detail {
#define ARGN 0
#include "syscall.inc"
#define ARGN 1
#include "syscall.inc"
#define ARGN 2
#include "syscall.inc"
#define ARGN 3
#include "syscall.inc"
#define ARGN 4
#include "syscall.inc"
#define ARGN 5
#include "syscall.inc"
#define ARGN 6
#include "syscall.inc"

	static_assert(sizeof(size_t) == 8);
	static_assert(sizeof(ssize_t) == 8);
	static_assert(sizeof(void *) == 8);

	template <typename... Args>
	struct check_args {};

	template <>
	struct check_args<> {};

	template <typename T, typename... Args>
	struct check_args<T, Args...> { static_assert(sizeof(T) == 8, "Invalid system call argument"); };

	template <typename... Args>
	ssize_t syscall(int nr, Args... args) {
		check_args<Args...>();
		return real_syscall(nr, (size_t)args...);
	}
} // detail

enum class fd_t: size_t {};
static constexpr fd_t invalid_fd = (fd_t) -1;

inline value_or_error read(fd_t fd, buffer_ref buf) {
	return value_or_error{detail::syscall(__NR_read, fd, buf.data, buf.size)};
}

inline value_or_error write(fd_t fd, const_buffer_ref buf) {
	return value_or_error{detail::syscall(__NR_write, fd, buf.data, buf.size)};
}

} // unistd
} // nz
