#if ARGN < 0
#error Negative argument number...
#endif

#if ARGN >= 7
#error Too many arguments to a system call
#endif

inline ssize_t real_syscall(int nr
#if ARGN >= 1
		, size_t arg1
#endif
#if ARGN >= 2
		, size_t arg2
#endif
#if ARGN >= 3
		, size_t arg3
#endif
#if ARGN >= 4
		, size_t arg4
#endif
#if ARGN >= 5
		, size_t arg5
#endif
#if ARGN >= 6
		, size_t arg6
#endif
		) {
	register size_t rax asm("rax") = nr;
#if ARGN >= 1
	register size_t rdi asm("rdi") = arg1;
#endif
#if ARGN >= 2
	register size_t rsi asm("rsi") = arg2;
#endif
#if ARGN >= 3
	register size_t rdx asm("rdx") = arg3;
#endif
#if ARGN >= 4
	register size_t r10 asm("r10") = arg4;
#endif
#if ARGN >= 5
	register size_t r8 asm("r8") = arg5;
#endif
#if ARGN >= 6
	register size_t r9 asm("r9") = arg6;
#endif
	__asm__ volatile ("syscall" : "=a" (rax) :
#if ARGN >= 1
		"r" (rdi),
#endif
#if ARGN >= 2
		"r" (rsi),
#endif
#if ARGN >= 3
		"r" (rdx),
#endif
#if ARGN >= 4
		"r" (r10),
#endif
#if ARGN >= 5
		"r" (r8),
#endif
#if ARGN >= 6
		"r" (r9),
#endif
		"a" (rax) :
#if ARGN < 1
		"rdi",
#endif
#if ARGN < 2
		"rsi",
#endif
#if ARGN < 3
		"rdx",
#endif
#if ARGN < 4
		"r10",
#endif
#if ARGN< 5
		"r8",
#endif
#if ARGN < 6
		"r9",
#endif
		"r11",
		"rcx",
		"cc", "memory");
	return rax;
}
#undef ARGN
