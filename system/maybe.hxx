#pragma once
#include <system_error>
#include "types.hxx"

namespace nz {

struct value_or_error {
	value_or_error() = default;
	value_or_error(value_or_error const &) = default;
	explicit value_or_error(ssize_t _v) noexcept : v(_v) {}

	size_t value() const {
		if (error())
			throw std::system_error(error(), std::generic_category());
		return v;
	}

	size_t error() const noexcept {
		if (v >= 0)
			return 0;
		return -v;
	}

	operator size_t() const {
		return value();
	}

	explicit operator bool() const noexcept {
		return !error();
	}

private:
	ssize_t v = 0;
};

} // nz
