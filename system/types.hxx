#pragma once
#include <cstdint>
#include <type_traits>

namespace nz {

using ::std::size_t;
using ssize_t = std::make_signed<size_t>::type;
using c_string = char const *;
enum class byte: std::uint8_t {};

struct const_buffer_ref {
	byte const *data;
	size_t size;

	const_buffer_ref() = default;

	const_buffer_ref(void const *d, size_t s)
			: data(reinterpret_cast<byte const *>(d))
			, size(s)
	{
	}

	const_buffer_ref(byte const *d, size_t s)
			: data(d)
			, size(s)
	{
	}
};

struct buffer_ref {
	byte *data;
	size_t size;

	buffer_ref() = default;

	buffer_ref(void *d, size_t s)
			: data(reinterpret_cast<byte *>(d))
			, size(s)
	{
	}

	buffer_ref(byte *d, size_t s)
			: data(d)
			, size(s)
	{
	}

	operator const_buffer_ref() const {
		return { data, size };
	}
};

template <typename U>
buffer_ref ref_object(U &object) {
	return buffer_ref(&object, sizeof(U));
}

template <typename U>
const_buffer_ref ref_object(U const &object) {
	return const_buffer_ref(&object, sizeof(U));
}

}
