# LibNZ

This is an auxiliary library intended to augment the C++ standard library.
It relies on modern language features heavily. It won’t magically make C++
into something different; actually, it should make it even *more* C++y.
Nevertheless, it should be interoperable with C, where appropriate.

## File naming conventions
* `.hxx`: user include
* `.cxx`: library source, if appropriate
* `.hpp`: template declarations
* `.cpp`: template definitions
